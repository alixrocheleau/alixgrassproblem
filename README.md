# alixGrassProblem

## Getting started

You will need GM:S 2 to open the source to compile.
Or run the .exe to view an example.

## Description
Grass shader, grass rendering. The problem is simple, you need to draw lots of images to simulate grass, but you can't do it in-engine because that would cost way too much. You have to draw it with the GPU using vertex buffers.

[Nvidia post](https://developer.nvidia.com/gpugems/gpugems/part-i-natural-effects/chapter-7-rendering-countless-blades-waving-grass)
[GLSL](http://www.lighthouse3d.com/tutorials/glsl-tutorial/vertex-shader/)

## Images

![img2](/images/grassgif.gif)
![img1](/images/grassDense.png)
![img3](/images/grassMedium.png)
![img4](/images/grassSparse.png)
![img5](/images/grassZones.png)