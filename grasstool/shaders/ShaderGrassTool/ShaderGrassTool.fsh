varying vec2 v_vTexcoord;
varying vec2 uv_start;
varying vec2 uv_end;
varying vec4 v_vColour;
varying vec2 room_pos;

uniform float time;
uniform float wave_dist;
uniform float slow;
uniform float wind_bias;

void main()
{	
	vec2 Texcoord = v_vTexcoord;	
	Texcoord = vec2((Texcoord-uv_start) * (1.0/uv_end));
	float offset = (sin((time/slow) + (room_pos.y)) * wave_dist) * (1.0-Texcoord.y);
	float wind = (wind_bias/20.0) * (1.0-Texcoord.y);
	vec2 txc = v_vTexcoord + vec2(offset - wind, 0);
	gl_FragColor = v_vColour * texture2D(gm_BaseTexture, txc);
	if (gl_FragColor.a==0.0 || txc.x < uv_start.x || txc.x > uv_start.x + uv_end.x || txc.y < uv_start.y || txc.y > uv_start.y + uv_end.y){
		discard;	
	}
}
