gpu_set_alphatestenable(true);
if (HasWind){
	shader_set(ShaderGrassTool);	
	shader_set_uniform_f(UniformTime, current_time);
	shader_set_uniform_f(UniformWaveDistance, 0.02 * WindPower);
	shader_set_uniform_f(UniformSlow, 200 / Speed);
	shader_set_uniform_f(UniformWindBias, WindHorizontalBias);
}
vertex_submit(vbuff, pr_trianglelist, tex);
if (HasWind){
	shader_reset();	
}	
gpu_set_alphatestenable(false);