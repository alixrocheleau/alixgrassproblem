/// @description description
//Mode=GrassMode.wholeroom;
Radius=sprite_width/2;
Width=sprite_width;
Height=sprite_height;
//Density=1; //Define in room editor per object, higher=more grass
Image=spr_grass;
CanRandom=true;
HasWind=true;
WindPower=1;
Speed=1;
WindHorizontalBias=0;

_grassDepthOffset=192+6;
_amountOfGrass=0;
_defaultSparsity=2000;

if (Mode==GrassMode.wholeroom){
		_amountOfGrass=(room_width*room_height)/_defaultSparsity * Density;
	}else{
		_amountOfGrass=(Width*Height)/_defaultSparsity * Density;		
	}
if(HasWind){
	UniformTime = shader_get_uniform(ShaderGrassTool, "time");
	UniformWaveDistance = shader_get_uniform(ShaderGrassTool, "wave_dist");
	UniformSlow = shader_get_uniform(ShaderGrassTool, "slow");
	UniformWindBias = shader_get_uniform(ShaderGrassTool, "wind_bias");	
}
gpu_set_ztestenable(true);
vertex_format_begin();
vertex_format_add_position_3d();
vertex_format_add_texcoord();
vertex_format_add_custom(vertex_type_float2, vertex_usage_texcoord); //uv
vertex_format_add_custom(vertex_type_float2, vertex_usage_texcoord); //uv
vertex_format_add_custom(vertex_type_float2, vertex_usage_texcoord); //room xy
vertex_format_add_color();
var format = vertex_format_end();
vbuff = vertex_create_buffer();
vertex_begin(vbuff, format);
repeat(_amountOfGrass){
	var ind = 0;
	if (CanRandom) ind = irandom(sprite_get_number(Image)-1);
	tex = sprite_get_texture(Image, ind);
	var uvs = sprite_get_uvs(Image, ind);
	var uvLeft = uvs[0];
	var uvTop = uvs[1];
	var uvRight = uvs[2];
	var uvBottom = uvs[3];
	var imageW = sprite_get_width(Image);
	var imageH = sprite_get_height(Image);	
	var left=0,top=0;
	
	if (Mode==GrassMode.wholeroom){
	     left = random_range(-imageW, room_width);
	     top = random_range(-imageH, room_height);
	}else if (Mode==GrassMode.radius){
		var rndDist = random(Radius);
		var rndDir = random(360);		
		left = x+Radius-imageW/2 + lengthdir_x(rndDist, rndDir);
		top = y+Radius-imageH/2 + lengthdir_y(rndDist, rndDir);
	}else if (Mode==GrassMode.rectangle){
		 left = random_range(x-imageW/2, x + Width-imageW/2);
		 top = random_range(y-imageH/2, y + Height-imageH/2);
	}	
    var right = left + imageW;
    var bottom = top + imageH;
	var centerX = left + imageW/2;
	var centerY = top + imageH/2;	
    var grassDepth = -(top+_grassDepthOffset);	
    //triangle 1
    vertex_position_3d(vbuff, left, top, grassDepth);
    vertex_texcoord(vbuff, uvLeft, uvTop);
	vertex_texcoord(vbuff, uvLeft, uvTop);
	vertex_texcoord(vbuff, uvRight - uvLeft, uvBottom - uvTop);
	vertex_texcoord(vbuff, centerX, centerY);
    vertex_colour(vbuff, c_white, 1);    
	
    vertex_position_3d(vbuff, right, top, grassDepth);
    vertex_texcoord(vbuff, uvRight, uvTop);
	vertex_texcoord(vbuff, uvLeft, uvTop);
	vertex_texcoord(vbuff, uvRight - uvLeft, uvBottom - uvTop);
	vertex_texcoord(vbuff, centerX, centerY);
    vertex_colour(vbuff, c_white, 1);
    
    vertex_position_3d(vbuff, left, bottom, grassDepth);
    vertex_texcoord(vbuff, uvLeft, uvBottom);
	vertex_texcoord(vbuff, uvLeft, uvTop);
	vertex_texcoord(vbuff, uvRight - uvLeft, uvBottom - uvTop);
	vertex_texcoord(vbuff, centerX, centerY);
    vertex_colour(vbuff, c_white, 1);
    
    //triangle 2
    vertex_position_3d(vbuff, right, top, grassDepth);
    vertex_texcoord(vbuff, uvRight, uvTop);
	vertex_texcoord(vbuff, uvLeft, uvTop);
	vertex_texcoord(vbuff, uvRight - uvLeft, uvBottom - uvTop);
	vertex_texcoord(vbuff, centerX, centerY);
    vertex_colour(vbuff, c_white, 1);
    
    vertex_position_3d(vbuff, right, bottom, grassDepth);
    vertex_texcoord(vbuff, uvRight, uvBottom);
	vertex_texcoord(vbuff, uvLeft, uvTop);
	vertex_texcoord(vbuff, uvRight - uvLeft, uvBottom - uvTop);
	vertex_texcoord(vbuff, centerX, centerY);
    vertex_colour(vbuff, c_white, 1);
    
    vertex_position_3d(vbuff, left, bottom, grassDepth);
    vertex_texcoord(vbuff, uvLeft, uvBottom);
	vertex_texcoord(vbuff, uvLeft, uvTop);
	vertex_texcoord(vbuff, uvRight - uvLeft, uvBottom - uvTop);
	vertex_texcoord(vbuff, centerX, centerY);
    vertex_colour(vbuff, c_white, 1);
}

vertex_end(vbuff);
vertex_freeze(vbuff);